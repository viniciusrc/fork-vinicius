﻿using AngularMVC4.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularMVC4.Dao
{
    public class StatusDao
    {
        public IEnumerable retornaTodosStatus()
        {
            var db = new PSDBContext();

            IEnumerable status = db.Status.ToList();

            db.Dispose();

            return status;
        }
    }
}