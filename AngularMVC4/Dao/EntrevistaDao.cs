﻿using AngularMVC4.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularMVC4.Dao
{
    public class EntrevistaDao
    {
        public IEnumerable retornaTodasEntrevistas()
        {

            var db = new PSDBContext();

            var entrevistas = (from ent in db.Entrevistas
                               where ent.id_entrevista != null
                               orderby ent.id_entrevista descending
                               select new { entrevistador = ent.entrevistador, id_entrevista = ent.id_entrevista, eleitores = ent.eleitores, municipio = ent.municipio, pessoa_entrevistada = ent.pessoa_entrevistada, televisao = ent.televisao, blog = ent.blog, ent.uf, populacao = ent.populacao }).ToList();

            db.Dispose();

            return entrevistas;

        }

        public Entrevista retornaEntrevistaPorId(int id_entrevista)
        {
            var db = new PSDBContext();

            Entrevista entrevista = db.Entrevistas.SingleOrDefault<Entrevista>(ent => ent.id_entrevista == id_entrevista);

            db.Dispose();

            return entrevista;
        }

        public IEnumerable retornaEntrevistasMaiores(int quantidade)
        {

            var db = new PSDBContext();

            IEnumerable<Entrevista> sql_entrevistas = db.Database.SqlQuery<Entrevista>("SELECT TOP "+quantidade+" id_entrevista, id_chance, id_usuario, id_status, entrevistador, telefone_entrevistado, populacao, eleitores, prefeito, partido, coligacao, pre_condidato, uf, municipio, televisao, blog, deputado_federal, meta_vereadores, pessoa_entrevistada FROM Entrevistas order by populacao desc");

            return sql_entrevistas;


        }

        public IEnumerable retornaEntrevistaPreCandidatos(bool is_pre_candidato)
        {
            var db = new PSDBContext();

            IEnumerable entrevistas = null;

            
                entrevistas = (from ent in db.Entrevistas
                               where ent.pre_condidato == is_pre_candidato
                               orderby ent.id_entrevista descending
                               select new { entrevistador = ent.entrevistador, id_entrevista = ent.id_entrevista, eleitores = ent.eleitores, municipio = ent.municipio, pessoa_entrevistada = ent.pessoa_entrevistada, televisao = ent.televisao, blog = ent.blog, ent.uf, populacao = ent.populacao }).ToList();

                return entrevistas;
        }

        public IEnumerable retornaEntrevistaEspecifica(string partido, string municipio, string uf, int status)
        {
            var db = new PSDBContext();

            IEnumerable entrevistas = null;

            if (partido == null && municipio == null && uf == null)
            {
                entrevistas = (from ent in db.Entrevistas
                               where ent.id_status == status
                               orderby ent.id_entrevista descending
                               select new { entrevistador = ent.entrevistador, id_entrevista = ent.id_entrevista, eleitores = ent.eleitores, municipio = ent.municipio, pessoa_entrevistada = ent.pessoa_entrevistada, televisao = ent.televisao, blog = ent.blog, ent.uf, populacao = ent.populacao }).ToList();

            }

            if (partido != null && municipio == null && uf == null)
            {
                entrevistas = (from ent in db.Entrevistas
                               where ent.id_status == status && ent.partido.Contains(partido)
                               orderby ent.id_entrevista descending
                               select new { entrevistador = ent.entrevistador, id_entrevista = ent.id_entrevista, eleitores = ent.eleitores, municipio = ent.municipio, pessoa_entrevistada = ent.pessoa_entrevistada, televisao = ent.televisao, blog = ent.blog, ent.uf, populacao = ent.populacao }).ToList();
            }

            if (partido == null && municipio != null && uf == null)
            {
                entrevistas = (from ent in db.Entrevistas
                               where ent.id_status == status && ent.municipio.Contains(municipio)
                               orderby ent.id_entrevista descending
                               select new { entrevistador = ent.entrevistador, id_entrevista = ent.id_entrevista, eleitores = ent.eleitores, municipio = ent.municipio, pessoa_entrevistada = ent.pessoa_entrevistada, televisao = ent.televisao, blog = ent.blog, ent.uf, populacao = ent.populacao }).ToList();

            }

            if (partido == null && municipio == null && uf != null)
            {
                entrevistas = (from ent in db.Entrevistas
                               where ent.id_status == status && ent.uf.Contains(uf)
                               orderby ent.id_entrevista descending
                               select new { entrevistador = ent.entrevistador, id_entrevista = ent.id_entrevista, eleitores = ent.eleitores, municipio = ent.municipio, pessoa_entrevistada = ent.pessoa_entrevistada, televisao = ent.televisao, blog = ent.blog, ent.uf, populacao = ent.populacao }).ToList();

            }

            if (partido != null && municipio != null && uf == null)
            {
                entrevistas = (from ent in db.Entrevistas
                               where ent.id_status == status && ent.partido.Contains(partido) && ent.municipio.Contains(municipio)
                               orderby ent.id_entrevista descending
                               select new { entrevistador = ent.entrevistador, id_entrevista = ent.id_entrevista, eleitores = ent.eleitores, municipio = ent.municipio, pessoa_entrevistada = ent.pessoa_entrevistada, televisao = ent.televisao, blog = ent.blog, ent.uf, populacao = ent.populacao }).ToList();

            }

            if (partido == null && municipio != null && uf != null)
            {
                entrevistas = (from ent in db.Entrevistas
                               where ent.id_status == status && ent.municipio.Contains(municipio) && ent.uf.Contains(uf)
                               orderby ent.id_entrevista descending
                               select new { entrevistador = ent.entrevistador, id_entrevista = ent.id_entrevista, eleitores = ent.eleitores, municipio = ent.municipio, pessoa_entrevistada = ent.pessoa_entrevistada, televisao = ent.televisao, blog = ent.blog, ent.uf, populacao = ent.populacao }).ToList();

            }


            if (partido != null && municipio != null && uf != null)
            {
                entrevistas = (from ent in db.Entrevistas
                               where ent.id_status == status && ent.municipio.Contains(municipio) && ent.uf.Contains(uf) && ent.partido.Contains(partido)
                               orderby ent.id_entrevista descending
                               select new { entrevistador = ent.entrevistador, id_entrevista = ent.id_entrevista, eleitores = ent.eleitores, municipio = ent.municipio, pessoa_entrevistada = ent.pessoa_entrevistada, televisao = ent.televisao, blog = ent.blog, ent.uf, populacao = ent.populacao }).ToList();

            }

            db.Dispose();

            return entrevistas;
        }

        public void salvaNovaEntrevista(Entrevista entrevista_nova)
        {

            var db = new PSDBContext();

            Entrevista entrevista = db.Entrevistas.Add(entrevista_nova);

            db.SaveChanges();

            db.Dispose();

        }

        public void alteraEntrevista(Entrevista entrevista_update)
        {

            var db = new PSDBContext();

            db.Entry(entrevista_update).State = System.Data.EntityState.Modified;

            db.SaveChanges();

            db.Dispose();
        }
    }
}