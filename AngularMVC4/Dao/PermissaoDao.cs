﻿using AngularMVC4.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularMVC4.Dao
{
    public class PermissaoDao
    {
        public IEnumerable retornaTodasPermissoes()
        {
            var db = new PSDBContext();

            IEnumerable permissoes = db.Permissoes.ToList();

            db.Dispose();

            return permissoes;
        }

        public IEnumerable retornaPermissoesPorTipoUsuario(int id_tipo_usuario)
        {

            var db = new PSDBContext();

            TiposUsuario tipo = db.TiposUsuarios.Where(tp => tp.id_tipo_usuario == id_tipo_usuario).SingleOrDefault<TiposUsuario>();

            LinkedList<Permisso> permissoes_usuario = new LinkedList<Permisso>();

            foreach (Permisso per in tipo.Permissoes)
            {
                Permisso per_usu = new Permisso();

                per_usu.id_permissao = per.id_permissao;
                per_usu.descricao = per.descricao;

                permissoes_usuario.AddFirst(per_usu);
            }

            db.Dispose();

            return permissoes_usuario;
        }



    }
}