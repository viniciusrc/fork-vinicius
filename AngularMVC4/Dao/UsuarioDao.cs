﻿using AngularMVC4.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularMVC4.Dao
{
    public class UsuarioDao
    {

        public IEnumerable retornaUsuarios()
        {
            var db = new PSDBContext();

            var usuarios = (from usu in db.Usuarios
                            where usu.id_usuario != null
                            orderby usu.id_usuario descending
                            select new { id_usuario = usu.id_usuario, nome = usu.nome, data_ultimo_acesso = usu.data_ultimo_acesso, id_tipo_usuario = usu.id_tipo_usuario, login = usu.login }).ToList();

            db.Dispose();

            return usuarios;

        }

        public IEnumerable retornaUsuariosEspecificos(string login, string nome, int tipo)
        {
            var db = new PSDBContext();

            IEnumerable usuarios = null;

            if (login == null && nome == null)
            {
                usuarios = (from usu in db.Usuarios
                            where usu.id_tipo_usuario == tipo
                            orderby usu.id_usuario descending
                            select new { id_usuario = usu.id_usuario, nome = usu.nome, data_ultimo_acesso = usu.data_ultimo_acesso, id_tipo_usuario = usu.id_tipo_usuario, login = usu.login }).ToList();

            }

            if (login != null && nome == null)
            {
                usuarios = (from usu in db.Usuarios
                            where usu.id_tipo_usuario == tipo && usu.login.Contains(login)
                            orderby usu.id_usuario descending
                            select new { id_usuario = usu.id_usuario, nome = usu.nome, data_ultimo_acesso = usu.data_ultimo_acesso, id_tipo_usuario = usu.id_tipo_usuario, login = usu.login }).ToList();

            }

            if (login == null && nome != null)
            {
                usuarios = (from usu in db.Usuarios
                            where usu.id_tipo_usuario == tipo && usu.nome.Contains(nome)
                            orderby usu.id_usuario descending
                            select new { id_usuario = usu.id_usuario, nome = usu.nome, data_ultimo_acesso = usu.data_ultimo_acesso, id_tipo_usuario = usu.id_tipo_usuario, login = usu.login }).ToList();

            }

            if (login != null && nome != null)
            {
                usuarios = (from usu in db.Usuarios
                            where usu.id_tipo_usuario == tipo && login.Contains(login) && usu.nome.Contains(nome)
                            orderby usu.id_usuario descending
                            select new { id_usuario = usu.id_usuario, nome = usu.nome, data_ultimo_acesso = usu.data_ultimo_acesso, id_tipo_usuario = usu.id_tipo_usuario, login = usu.login }).ToList();

            }
           
            db.Dispose();

            return usuarios;

        }

        public void criaUsuario(Usuario novo_usuario) {

            using (PSDBContext db = new PSDBContext())
            {
                db.Usuarios.Add(novo_usuario);

                db.SaveChanges();
                
            }
        }

        public Usuario retornaUsuarioPorCredenciais(string login, string senha)
        {

            using (PSDBContext db = new PSDBContext())
            {

                Usuario usuario = db.Usuarios.Where(usu => usu.login == login && usu.senha == senha).FirstOrDefault<Usuario>();
                //select new { data_ultimo_acesso = usu.data_ultimo_acesso, id_tipo_usuario = usu.id_tipo_usuario, login = usu.login, senha = usu.senha, trocou_senha = usu.trocou_senha };

                return usuario;
            }
        }

        public void alteraUsuario(Usuario usuario)
        {
            using (PSDBContext db = new PSDBContext())
            {

                Usuario usuario_update = db.Usuarios.Where(usu => usu.id_usuario == usuario.id_usuario).SingleOrDefault<Usuario>();

                usuario_update.nome = usuario.nome;

                usuario_update.login = usuario.login;

                usuario_update.trocou_senha = true;

                usuario_update.senha = usuario.senha;

                usuario_update.id_tipo_usuario = usuario.id_tipo_usuario;

                db.Entry(usuario_update).State = System.Data.EntityState.Modified;

                db.SaveChanges();

            }
        }

        public void alteraSenhaUsuario(Usuario usuario)
        {
            using (PSDBContext db = new PSDBContext())
            {

                Usuario usuario_update = db.Usuarios.Where(usu => usu.id_usuario == usuario.id_usuario).FirstOrDefault<Usuario>();

                if (usuario_update.senha == usuario.senha)
                {
                    throw new Exception("Não houve mudança de senha");
                }

                usuario_update.senha = usuario.senha;

                usuario_update.trocou_senha = true;

                usuario_update.data_ultimo_acesso = DateTime.Now;

                db.Entry(usuario_update).State = System.Data.EntityState.Modified;

                db.SaveChanges();

            }
        }

        public Usuario retornaUsuarioPorId(int id_usuario) {

            using (PSDBContext db = new PSDBContext())
            {

                Usuario usuario = db.Usuarios.Where(usu => usu.id_usuario == id_usuario).SingleOrDefault<Usuario>();

                return usuario;
            }
        }

        public void deletaUsuario(int id_usuario) {
            using (PSDBContext db = new PSDBContext())
            {

                Usuario usuario = db.Usuarios.Where(usu => usu.id_usuario == id_usuario).SingleOrDefault<Usuario>();

                db.Usuarios.Remove(usuario);

                db.SaveChanges();

               
            }
        }

    }
}