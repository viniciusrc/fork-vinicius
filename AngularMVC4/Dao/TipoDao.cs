﻿using AngularMVC4.Models;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AngularMVC4.Dao
{
    public class TipoUsuarioDao
    {
        private PSDBContext db;

        public TipoUsuarioDao(PSDBContext db)
        {
            this.db = db;
        }

        public IEnumerable retornaTodasTipos()
        {
            IEnumerable tipos = db.TiposUsuarios.ToList();

            LinkedList<TiposUsuario> tipos_usuario = new LinkedList<TiposUsuario>();

            foreach (TiposUsuario tipo in tipos)
            {
                TiposUsuario tipo_usu = new TiposUsuario();

                tipo_usu.id_tipo_usuario = tipo.id_tipo_usuario;
                tipo_usu.descricao = tipo.descricao;

                tipos_usuario.AddFirst(tipo_usu);
            }

            return tipos_usuario;
        }
       
        public TiposUsuario repornaPorId(int id)
        {

            db.Configuration.LazyLoadingEnabled = false;

            var tipo_usuario = db.TiposUsuarios.Where(tpu => tpu.id_tipo_usuario == id).Single<TiposUsuario>();

            return tipo_usuario;

        }

        public void alteraTipoUsuario(TiposUsuario tipo_usuario_update)
        {
            db.Entry<TiposUsuario>(tipo_usuario_update).State = System.Data.EntityState.Modified;
            db.SaveChanges();
        }

        public void criaTipoUsuario(TiposUsuario tipo_usuario_create)
        {

            db = new PSDBContext();

            int ultimo_id = db.TiposUsuarios.Max(tpu => tpu.id_tipo_usuario);

            tipo_usuario_create.id_tipo_usuario = ++ ultimo_id;

            db.Entry<TiposUsuario>(tipo_usuario_create).State = System.Data.EntityState.Added;

            db.SaveChanges();

            db.Dispose();
        }


        public void deletaPorId(int id)
        {

            db = new PSDBContext();

            db.Configuration.LazyLoadingEnabled = false;

            var tipo_usuario = db.TiposUsuarios.Where(tpu => tpu.id_tipo_usuario == id).Single<TiposUsuario>();

            db.Entry<TiposUsuario>(tipo_usuario).State = System.Data.EntityState.Deleted;

            db.SaveChanges();

            db.Dispose();

        }
    }
}