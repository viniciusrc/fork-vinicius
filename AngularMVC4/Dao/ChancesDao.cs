﻿using AngularMVC4.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularMVC4.Dao
{
    public class ChancesDao
    {
        public IEnumerable retornaTodasChances()
        {
            var db = new PSDBContext();

            IEnumerable chances = db.Chances.ToList();

            db.Dispose();

            return chances;
        }
    }
}