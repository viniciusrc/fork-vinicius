﻿using AngularMVC4.Dao;
using AngularMVC4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Net;
using Newtonsoft.Json;
using AngularMVC4.Dao;
using AngularMVC4.Models;
using System.Collections;

namespace AngularMVC4.Controllers
{
    public class EntrevistaController : ApiController
    {

        public HttpResponseMessage Get() {

            try
            {
                EntrevistaDao entrevista_dao = new Dao.EntrevistaDao();

                IEnumerable entrevistas = entrevista_dao.retornaTodasEntrevistas();

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, entrevistas);

            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }

        public HttpResponseMessage GetEntrevista(int id_entrevista)
        {

            try
            {
                EntrevistaDao entrevista_dao = new Dao.EntrevistaDao();

                Entrevista entrevista = entrevista_dao.retornaEntrevistaPorId(id_entrevista);

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, entrevista);

            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }

        [HttpGet]
        public HttpResponseMessage Especifica(string partido, string municipio, string uf, int status)
        {

            try
            {
                EntrevistaDao entrevista_dao = new Dao.EntrevistaDao();

                IEnumerable entrevistas = entrevista_dao.retornaEntrevistaEspecifica(partido, municipio, uf, status);

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, entrevistas);

            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }

        [HttpGet]
        public HttpResponseMessage Maiores(string quantidade)
        {

            try
            {
                EntrevistaDao entrevista_dao = new Dao.EntrevistaDao();

                IEnumerable entrevistas = entrevista_dao.retornaEntrevistasMaiores(Int16.Parse(quantidade));

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, entrevistas);

            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }


        [HttpGet]
        public HttpResponseMessage PreCandidato(bool pre_candidato)
        {
            try
            {
                EntrevistaDao entrevista_dao = new Dao.EntrevistaDao();

                IEnumerable entrevistas = entrevista_dao.retornaEntrevistaPreCandidatos(pre_candidato);

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, entrevistas);

            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }

        [HttpPost]
        public HttpResponseMessage Criar(Entrevista entrevista_body)
        {
            try
            {

                EntrevistaDao entrevista_dao = new Dao.EntrevistaDao();

                entrevista_dao.salvaNovaEntrevista(entrevista_body);               

                return Request.CreateResponse(System.Net.HttpStatusCode.OK);


            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }
        }

        [HttpPut]
        public HttpResponseMessage AlteraEntrevista(Entrevista entrevista_update)
        {
            try
            {                

                EntrevistaDao entrevista_dao = new Dao.EntrevistaDao();

                entrevista_dao.alteraEntrevista(entrevista_update);               

                return Request.CreateResponse(System.Net.HttpStatusCode.OK);


            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }
        }
    }
}
