﻿using AngularMVC4.Dao;
using AngularMVC4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Net;
using Newtonsoft.Json;
using System.Collections;

namespace AngularMVC4.Controllers
{
    public class UsuarioController : ApiController
    {

        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {

                UsuarioDao usu_dao = new UsuarioDao();

                IEnumerable usuarios = usu_dao.retornaUsuarios();

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, usuarios);


            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }
        }

        [HttpGet]
        public HttpResponseMessage Especifico(string login, string nome, int tipo)
        {
            try
            {

                UsuarioDao usu_dao = new UsuarioDao();

                IEnumerable usuarios = usu_dao.retornaUsuariosEspecificos(login, nome, tipo);

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, usuarios);


            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }
        }

        [HttpGet]
        public HttpResponseMessage UsuarioPorId(int id_usuario)
        {
            try
            {

                UsuarioDao usu_dao = new UsuarioDao();

                Usuario usuario = usu_dao.retornaUsuarioPorId(id_usuario);

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, usuario);


            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }
        }

        [HttpPost]
        public HttpResponseMessage Criar(Usuario novo_usuario)
        {
            try
            {
                UsuarioDao usu_dao = new UsuarioDao();

                usu_dao.criaUsuario(novo_usuario);

                return Request.CreateResponse(System.Net.HttpStatusCode.OK);

            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }
        }
        [HttpPost]
        public HttpResponseMessage PorCredenciais(Usuario usuario_body, string login)
        {
            try
            {

                UsuarioDao usu_dao = new UsuarioDao();

                Usuario usuario = usu_dao.retornaUsuarioPorCredenciais(login, usuario_body.senha);

                if (usuario == null)
                {
                    return Request.CreateResponse(System.Net.HttpStatusCode.OK);
                }

                string json = JsonConvert.SerializeObject(usuario);

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, json);


            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }
        }

        [HttpPut]
        public HttpResponseMessage AlteraSenha(Usuario usuario_update)
        {
            try
            {
                UsuarioDao usu_dao = new UsuarioDao();

                usu_dao.alteraSenhaUsuario(usuario_update);

                return Request.CreateResponse(System.Net.HttpStatusCode.OK);


            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }
        }

        [HttpPut]
        public HttpResponseMessage AlteraUsuario(Usuario usuario_update, int id_usuario)
        {
            try
            {
                UsuarioDao usu_dao = new UsuarioDao();

                usu_dao.alteraUsuario(usuario_update);

                return Request.CreateResponse(System.Net.HttpStatusCode.OK);


            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }
        }

        [HttpDelete]
        public HttpResponseMessage Deletar(int id_usuario)
        {
            try
            {
                UsuarioDao usu_dao = new UsuarioDao();

                usu_dao.deletaUsuario(id_usuario);

                return Request.CreateResponse(System.Net.HttpStatusCode.OK);

            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }
        }


    }
}
