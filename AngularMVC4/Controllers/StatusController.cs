﻿using AngularMVC4.Dao;
using AngularMVC4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Net;
using Newtonsoft.Json;
using AngularMVC4.Dao;
using AngularMVC4.Models;
using System.Collections;

namespace AngularMVC4.Controllers
{
    public class StatusController : ApiController
    {

        public HttpResponseMessage Get() {

            try
            {
                StatusDao status_dao = new StatusDao();

                var status = status_dao.retornaTodosStatus();

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, status);

            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }

    }
}
