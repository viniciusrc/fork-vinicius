﻿using AngularMVC4.Dao;
using AngularMVC4.Models;
using System;
using System.Collections;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
namespace AngularMVC4.Controllers
{
    public class TipoController : ApiController
    {
        private PSDBContext db = null; 

        public HttpResponseMessage Get() {

            try
            {
                 db = new PSDBContext();

                TipoUsuarioDao tipo_dao = new Dao.TipoUsuarioDao(db);

                IEnumerable tipos = tipo_dao.retornaTodasTipos();               

                var resposta = Request.CreateResponse(System.Net.HttpStatusCode.OK, tipos);

                db.Dispose();

                return resposta;

            }
            catch (Exception e)
            {
                db.Dispose();

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }


        public HttpResponseMessage Get(int id)
        {

            try
            {
                db = new PSDBContext();

                TipoUsuarioDao tipo_dao = new Dao.TipoUsuarioDao(db);

                var tipo_usuario = tipo_dao.repornaPorId(id);

               // var json = JsonConvert.SerializeObject(tipo_usuario, Formatting.Indented);

                var resposta = Request.CreateResponse(System.Net.HttpStatusCode.OK, tipo_usuario);

                db.Dispose();

                return resposta;

            }
            catch (Exception e)
            {
                db.Dispose();

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }

        public HttpResponseMessage Put(TiposUsuario tipo_usuario)
        {

            try
            {
                db = new PSDBContext();

                TipoUsuarioDao tipo_dao = new Dao.TipoUsuarioDao(db);

                tipo_dao.alteraTipoUsuario(tipo_usuario);

                var resposta = Request.CreateResponse(System.Net.HttpStatusCode.NoContent);

                db.Dispose();

                return resposta;

            }
            catch (Exception e)
            {
                db.Dispose();

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }

        public HttpResponseMessage Post(TiposUsuario tipo_usuario)
        {

            try
            {

                TipoUsuarioDao tipo_dao = new Dao.TipoUsuarioDao(db);

                tipo_dao.criaTipoUsuario(tipo_usuario);

                var resposta = Request.CreateResponse(System.Net.HttpStatusCode.Created);

                return resposta;

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }

        public HttpResponseMessage Delete(int id)
        {

            try
            {

                TipoUsuarioDao tipo_dao = new Dao.TipoUsuarioDao(db);

                tipo_dao.deletaPorId(id);

                var resposta = Request.CreateResponse(System.Net.HttpStatusCode.OK);

                return resposta;

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }

    }
}
