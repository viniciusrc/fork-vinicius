﻿using AngularMVC4.Dao;
using AngularMVC4.Models;
using System;
using System.Collections;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
namespace AngularMVC4.Controllers
{
    public class PermissaoController : ApiController
    {
        private PSDBContext db = null; 

        public HttpResponseMessage Put(TiposUsuario tipo_usuario)
        {

            try
            {
                db = new PSDBContext();

                TipoUsuarioDao tipo_dao = new Dao.TipoUsuarioDao(db);

                tipo_dao.alteraTipoUsuario(tipo_usuario);

                var resposta = Request.CreateResponse(System.Net.HttpStatusCode.NoContent);

                db.Dispose();

                return resposta;

            }
            catch (Exception e)
            {
                db.Dispose();

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }
        
        [HttpGet]
        public HttpResponseMessage GetPermissoesTipoUsuario(int id_tipo_usuario)
        {

            try
            {
                PermissaoDao tipo_dao = new Dao.PermissaoDao();

                IEnumerable permissoes = tipo_dao.retornaPermissoesPorTipoUsuario(id_tipo_usuario);                

                var resposta = Request.CreateResponse(System.Net.HttpStatusCode.OK, permissoes);

                return resposta;

            }
            catch (Exception e)
            {
               
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }

    }
}
