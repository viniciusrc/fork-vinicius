﻿using AngularMVC4.Dao;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AngularMVC4.Controllers
{
    public class ChanceController : ApiController
    {

        public HttpResponseMessage Get() {

            try
            {
                ChancesDao chance_dao = new Dao.ChancesDao();

                var chances = chance_dao.retornaTodasChances();

                return Request.CreateResponse(System.Net.HttpStatusCode.OK, chances);

            }
            catch (Exception e)
            {

                var msg = e.Message;

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message, e);

            }

        }

    }
}
