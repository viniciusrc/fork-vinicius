﻿
ws_app.controller("ctrl_tipo_usuario", function ($scope, $http, $routeParams, $location, tipo_usuario_service) {


    $scope.loadAll = function () {

        var promise = tipo_usuario_service.listar();

        promise.then(function (response) {

            console.log(response.data);
            $scope.tipos_usuarios = response.data;

        }).catch(function (response) {
            console.log(response);

            alert(response.data.Message);
        })

    };


    $scope.loadOne = function () {

        var promise = tipo_usuario_service.buscarPorId($routeParams.index);

        promise.then(function (response) {

            console.log(response.data);

            $scope.tipo_usuario = response.data;
        }).catch(function (response) {
            console.log(response);

            alert(response.data.Message);
        })
    },

     $scope.updateRegister = function (tipo_usuario) {

         var promise = tipo_usuario_service.alterar(tipo_usuario);

         promise.then(function (response) {

             console.log(response.data);

             $location.path('/tipo_usuarios');

         }).catch(function (response) {
             console.log(response);

             alert(response.data.Message);
         })
     }

});
