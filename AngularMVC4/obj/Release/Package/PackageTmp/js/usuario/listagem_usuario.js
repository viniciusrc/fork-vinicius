﻿var array_tipo_usuarios = [];

$(function () {

    setTimeout(function () {
        GetTipoUsuarios();
        PreencheOptionSetTipoUsuario(20);
    }, 1400);

    $('#btn-pesquisar-usuario').click(function (event) {
        event.preventDefault();
        //PesquisaUsuariosEspecificos();
    });

    $('#btn-limpar-campos').click(function (event) {
        event.preventDefault();        
    });

});

function GetTipoUsuarios() {

    $.ajax({
        crossDomain: true,
        url: url_base + "/api/Tipo",
        data: null,
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        cache: true,
        async: false,
        success: function (data, XMLHttpRequest, jqXHR) {
            try {

                if (jqXHR.readyState == 4) {

                    debugger;
                    if (jqXHR.status == 200) {

                        console.log(data);

                        PreencheArrayTipoUsuario(data);


                    }

                    if (jqXHR.status == 400) {
                        debugger;
                        console.log(data);

                        alert('Erro no servidor');
                    }
                }

            } catch (e) {

                ApresentaErro(e, "GetTipoUsuarios");
            }

        },
        error: function (data, XMLHttpRequest, jqXHR) {

            debugger;
            console.log(data);
        }
    });
}

function PreencheArrayTipoUsuario(tipos) {
    try {

        for (var pos in tipos) {

            array_tipo_usuarios[tipos[pos].id_tipo_usuario] = tipos[pos].descricao;
        }

        $("tbody tr span").html();

        //$("tbody tr span").html(array_tipo_usuarios[posicao]);
        for (var pos in array_tipo_usuarios) {

            $(".tipo_" + pos).html(array_tipo_usuarios[pos]);

        }

    } catch (e) {

        ApresentaErro(e, "GetTipoUsuarios");
    }
}

function PreencheOptionSetTipoUsuario(tipo_selecionado) {

    $("#tipo").find('option').remove().end();

    for (var pos in array_tipo_usuarios) {

        if (pos == tipo_selecionado) {

            $("#tipo").append("<option selected='selected' id= '" + pos + "' value ='" + pos + "'  >" + array_tipo_usuarios[pos] + "</option>");
            continue;
        }

        $("#tipo").append('<option value = "' + pos + '">' + array_tipo_usuarios[pos] + '</option>');
    }
}

function PesquisaUsuariosEspecificos() {
    try {

        var tipo = $('#tipo').val();
        var login = $('#login').val();
        var nome = $('#nome').val();

        $.ajax({
            crossDomain: true,
            url: url_base + "/api/Usuario/Especifico?login=" + login + "&nome=" + nome + "&tipo=" + tipo + GetDummy(),
            data: null,
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            cache: false,
            async: false,
            success: function (data, XMLHttpRequest, jqXHR) {
                try {

                    if (jqXHR.readyState == 4) {


                        if (jqXHR.status == 200) {

                            console.log(data);
                            debugger;

                        }

                        if (jqXHR.status == 400) {

                            console.log(data);

                            alert('Erro no servidor');
                        }
                    }

                } catch (e) {

                    ApresentaErro(e, "PesquisaUsuariosEspecificos");
                }

            },
            error: function (data, XMLHttpRequest, jqXHR) {


                console.log(data.responseText);
                alert(data.responseText);
            }
        });

    } catch (e) {

        ApresentaErro(e, "PesquisaUsuariosEspecificos");
    }
}