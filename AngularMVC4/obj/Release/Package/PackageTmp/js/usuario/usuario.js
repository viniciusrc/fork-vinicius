﻿var usuario_objeto = new Object();
var array_tipo_usuarios = [];
var array_permissoes = [];
$(function () {
    $('#btn-salva-usuario').click(function () {

        
        Salvar();
    });

    $('#tipo').change(function () {
        ExibePermissoesTexto();
    });


});

function Salvar() {
    try {
       
        MontaObjetoUsuario();

        if ($("#id_usuario").val() != null && $("#id_usuario").val() != "") {

            EditaUsuario();
        } else {
            SalvaUsuario();
        }
    }
    catch (e) {

        ApresentaErro(e, "SalvaUsuario");
    }
}
function EditaUsuario() {
    try {


        $.ajax({
            crossDomain: true,
            url: url_base + "/api/Usuario/AlteraUsuario?id_usuario="+usuario_objeto.id_usuario,
            data: JSON.stringify(usuario_objeto),
            type: 'PUT',
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data, XMLHttpRequest, jqXHR) {
                try {

                    if (jqXHR.readyState == 4) {
                        debugger;
                        if (jqXHR.status != 200) {

                            alert('Erro do servidor!!!');
                            return;
                        }                       

                        console.log(data);

                        alert('Usuario alterado com sucesso!');

                    }

                } catch (e) {

                    ApresentaErro(e, "EditaUsuario");
                }

            },
            error: function (data, XMLHttpRequest, jqXHR) {
                debugger;
                var msg_erro = "";
                if (data.responseJSON.InnerException == undefined) {

                    msg_erro = data.responseJSON.ExceptionMessage;
                } else {
                    msg_erro = data.responseJSON.ExceptionMessage + "\n" + data.responseJSON.InnerException.ExceptionMessage;
                }

                console.log(msg_erro);

                alert(msg_erro);
            }
        });
    } catch (e) {

        ApresentaErro(e, "EditaUsuario");
    }
}
function SalvaUsuario() {
    try {

        if (!ValidouUsuario()) {
            return;
        }


        $.ajax({
            crossDomain: true,
            url: url_base + "/api/Usuario/Criar" ,
            data: JSON.stringify(usuario_objeto),
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data, XMLHttpRequest, jqXHR) {
                try {

                    if (jqXHR.readyState == 4) {
                        debugger;
                        if (jqXHR.status != 200) {

                            alert('Erro do servidor!!!');
                            return;
                        }

                        console.log(data);

                        alert('Usuario criado com sucesso!');

                        LimpaCamposUsuario();

                    }

                } catch (e) {

                    ApresentaErro(e, "SalvaUsuario");
                }

            },
            error: function (data, XMLHttpRequest, jqXHR) {
                debugger;
                var msg_erro = "";
                if (data.responseJSON.InnerException == undefined) {

                    msg_erro = data.responseJSON.ExceptionMessage;
                } else {
                    msg_erro = data.responseJSON.ExceptionMessage + "\n" + data.responseJSON.InnerException.ExceptionMessage;
                }

                console.log(msg_erro);

                alert(msg_erro);
            }
        });
    } catch (e) {

        ApresentaErro(e, "SalvaUsuario");
    }
}
function MontaObjetoUsuario() {

    usuario_objeto.nome = $("#nome").val();
    usuario_objeto.login = $("#login").val();
    usuario_objeto.senha = $("#senha").val();
    usuario_objeto.id_tipo_usuario = $("#tipo").val();

    if ($("#id_usuario").val() != null && $("#id_usuario").val() != "") {

        usuario_objeto.trocou_senha = true;
        usuario_objeto.id_usuario = $("#id_usuario").val();
    } else {

        usuario_objeto.trocou_senha = false;
        usuario_objeto.id_usuario = null;

    }

}

function ExibePermissoesTexto() {

    var tipo_usuario_atual = $("#tipo").val();
    
    $.ajax({
        crossDomain: true,
        url: url_base + "/api/Permissao/GetPermissoesTipoUsuario?id_tipo_usuario=" + tipo_usuario_atual,
        data: null,
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        cache: true,
        async: false,
        success: function (data, XMLHttpRequest, jqXHR) {
            try {

                if (jqXHR.readyState == 4) {


                    if (jqXHR.status == 200) {

                        console.log(data);

                        array_permissoes = data;
                        
                        PreencheDivPermissoes();
                    }

                    if (jqXHR.status == 400) {

                        console.log(data);
                        
                        alert('Erro no servidor');
                    }
                }

            } catch (e) {

                ApresentaErro(e, "ExibePermissoesTexto");
            }

        },
        error: function (data, XMLHttpRequest, jqXHR) {


            console.log(data);
        }
    });
}
function PreencheDivPermissoes() {
    try {

        $("#lista_permissoes").find('li').remove().end();

        for (var pos in array_permissoes) {

            $("#lista_permissoes").append("<li class='list-group list-group-item'><strong><span>" + array_permissoes[pos].descricao + "</span></strong></li>");

        }

    } catch (e) {

        ApresentaErro(e, "PreencheDivPermissoes");
    }
}

function PreencheOtpionSetsUsuario(usuario) {
    try {


        PreencheOptionSetTipoUsuario(usuario.id_tipo_usuario);



    } catch (e) {

        ApresentaErro(e, "PreencheOtpionSetsUsuario");
    }
}
function GetTipoUsuarios() {

    $.ajax({
        crossDomain: true,
        url: url_base + "/api/Tipo",
        data: null,
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        cache: true,
        async: false,
        success: function (data, XMLHttpRequest, jqXHR) {
            try {

                if (jqXHR.readyState == 4) {


                    if (jqXHR.status == 200) {

                        console.log(data);

                        array_tipo_usuarios = data;
                    }

                    if (jqXHR.status == 400) {

                        console.log(data);

                        alert('Erro no servidor');
                    }
                }

            } catch (e) {

                ApresentaErro(e, "GetTipoUsuarios");
            }

        },
        error: function (data, XMLHttpRequest, jqXHR) {


            console.log(data);
        }
    });
}

function PreencheOptionSetTipoUsuario(tipo_selecionado) {
    
    GetTipoUsuarios();

    $("#tipo").find('option').remove().end();

    for (var pos in array_tipo_usuarios) {

        if (array_tipo_usuarios[pos].id_tipo_usuario == tipo_selecionado) {

            $("#tipo").append("<option selected='selected' id= '" + array_tipo_usuarios[pos].id_tipo_usuario + "' value ='" + array_tipo_usuarios[pos].id_tipo_usuario + "'  >" + array_tipo_usuarios[pos].descricao + "</option>");
            continue;
        }

        $("#tipo").append('<option value = "' + array_tipo_usuarios[pos].id_tipo_usuario + '">' + array_tipo_usuarios[pos].descricao + '</option>');
    }
}
function ValidouUsuario() {
    if ($("#nome").val() == null || $("#nome").val() == "") {

        alert("Preencha o campo Nome");
        return false;
    }
    if ($("#login").val() == null || $("#login").val() == "") {

        alert("Preencha o campo Usuário");
        return false;
    }

    if ($("#senha").val() == null || $("#senha").val() == "") {

        alert("Preencha o campo Senha");
        return false;
    }


    if ($("#confirmacao_senha").val() == null || $("#confirmacao_senha").val() == "") {

        alert("Preencha o campo Confirmação de Senha");
        return false;
    }

    if ($("#confirmacao_senha").val() != $("#senha").val() ) {

        alert("Senha e confirmação de senha não conferem!");
        return false;
    }

    return true;
}
function LimpaCamposUsuario() {
    $("#nome").val(null);

    $("#login").val(null);

    $("#senha").val(null) ;

    $("#confirmacao_senha").val(null);

    $("#tipo").val(20);
}