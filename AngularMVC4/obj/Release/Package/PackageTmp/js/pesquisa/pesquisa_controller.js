﻿
ws_app.controller('ctrl_pesquisa', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {

    $scope.paginaAtual = 0;   
    $scope.tamanhoPagina = 4;

    $scope.numberOfPages = function () {
        return Math.ceil($scope.pesquisas.length / $scope.tamanhoPagina);
    }

    $scope.loadAll = function () {

        $http.get(url_base + "/api/Entrevista").success(function (data, status, fun, header) {
            
            $scope.pesquisas = data;
        }).error(function (data, status, fun, header) {
            
            console.log("PROBLEMA ==>: " + data);
        });
    }

    $scope.pesquisarMaiores = function () {

        debugger;

        $scope.pesquisas = [];

        $http.get(url_base + "/api/entrevista/Maiores?quantidade=3" + GetDummy()).success(function (data, status, fun, header) {

            if (data.length == 0) {
                alert('Não encontrou resgistros!');
            }

            debugger;
            $scope.pesquisas = data;
        }).error(function (data, status, fun, header) {
            debugger;
            console.log("PROBLEMA ==>: " + data.MessageDetail);
            alert(data.MessageDetail);
        });
    }

    $scope.pesquisarPreCandidatos = function () {

        debugger;

        $scope.pesquisas = [];

        $http.get(url_base + "/api/entrevista/PreCandidato?pre_candidato=true" + GetDummy()).success(function (data, status, fun, header) {

            if (data.length == 0) {
                alert('Não encontrou resgistros!');
            }

            debugger;
            $scope.pesquisas = data;
        }).error(function (data, status, fun, header) {
            debugger;
            console.log("PROBLEMA ==>: " + data.MessageDetail);
            alert(data.MessageDetail);
        });
    }


    $scope.pesquisarEspecifica = function () {
        
        var partido = ($scope.partido != undefined) ? $scope.partido : "";
        var municipio = ($scope.municipio != undefined) ? $scope.municipio : "";
        var uf = ($scope.uf != undefined) ? $scope.uf : "";
        var municipio = ($scope.municipio != undefined) ? $scope.municipio : "";
        var status = $("#status").val();

        $scope.pesquisas = [];
       
        $http.get(url_base + "/api/entrevista/Especifica?partido=" + partido + "&municipio=" + municipio + "&uf=" + uf + "&status=" + status + GetDummy()).success(function (data, status, fun, header) {

            if (data.length == 0) {
                alert('Não encontrou resgistros!');
            }

            debugger;
            $scope.pesquisas = data;
        }).error(function (data, status, fun, header) {
            debugger;
            console.log("PROBLEMA ==>: " + data.MessageDetail);
            alert(data.MessageDetail);
        });
    }

    $scope.limparCampos = function myfunction() {
        $scope.partido = null;
        $scope.municipio = null;
        $scope.uf = null;
    }

}])
 .controller('ctrl_entrevista_update', function ($scope,$http, $routeParams) {
     
    
     $http.get(url_base + "/api/Entrevista/GetEntrevista?id_entrevista="+$routeParams.index).success(function (data, status, fun, header) {
         
         $scope.entrevista = data;

         PreencheFormEntrevista($scope.entrevista);//From pesquisa.js

     }).error(function (data, status, fun, header) {
         
         console.log("PROBLEMA ==>: " + data);
     });
    

 })
.controller('ctrl_entrevista_nova', function ($scope,$http, $routeParams) {
     
    $scope.entrevista = null;

    var nova_entrevista = new Object();

    nova_entrevista.id_status = 1;//Em andamento

    nova_entrevista.id_chance = 10;//Não sei
    
    PreencheFormEntrevista(nova_entrevista);
});