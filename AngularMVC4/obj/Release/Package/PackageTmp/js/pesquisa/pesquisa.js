﻿var array_status = [];
var array_chances = [];
var entrevista_atual = new Object();
$(function () {
    $('#btn-sava-entrevista').click(function () {

        SalvarPesquisa();

    });
    $('#tel_responsavel').change(function () {

        ValidaTelefone('tel_responsavel');

    });

    $('#televisao').change(function () {
        cb = $(this);
        cb.val(cb.prop('checked'));

       
    });

    $('#blog').change(function () {
        cb = $(this);
        cb.val(cb.prop('checked'));

       
    });
    
    $('#pre_condidato').change(function () {
        cb = $(this);
        cb.val(cb.prop('checked'));


    });
});
function SalvarPesquisa() {
    try {

        if ($("#id_entrevista").val() == null || $("#id_entrevista").val() == "") {

            AdicionaNovaEntrevista();
        } else {
            EditaEntrevista();
        }        

    } catch (e) {

        ApresentaErro(e, "SalvarPesquisa");
    }
}
function AdicionaNovaEntrevista() {
    try {
        
        if (!ValidouEntrevista()) {
            return;
        }

        var nova_entrevista = MontaObjetoEntrevista();
        $.ajax({
            crossDomain: true,
            url: url_base + "/api/Entrevista/Criar",
            data: JSON.stringify(nova_entrevista),
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data, XMLHttpRequest, jqXHR) {
                try {

                    if (jqXHR.readyState == 4) {

                        if (jqXHR.status != 200) {
                            
                            alert('Erro!!!');
                            return;
                        }

                        console.log(data);

                        alert('Salvo com sucesso!');

                        window.location.href = url_base + "#/pesquisas";
                    }

                } catch (e) {

                    ApresentaErro(e, "EditaEntrevista");
                }

            },
            error: function (data, XMLHttpRequest, jqXHR) {

                var msg_erro = "";
                if (data.responseJSON.InnerException == undefined) {

                    msg_erro = data.responseJSON.ExceptionMessage;
                } else {
                    msg_erro = data.responseJSON.ExceptionMessage + "\n" + data.responseJSON.InnerException.ExceptionMessage;
                }

                console.log(msg_erro);

                alert(msg_erro);
            }
        });

        
    } catch (e) {

        ApresentaErro(e, "AdicionaNovaEntrevista");
    }
}
function EditaEntrevista() {
    try {

        if (!ValidouEntrevista()) {
            return;
        }

        var entrevista_update = MontaObjetoEntrevista();
        
        $.ajax({
            crossDomain: true,
            url: url_base + "/api/Entrevista/AlteraEntrevista",
            data: JSON.stringify(entrevista_update),
            type: 'PUT',
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (data, XMLHttpRequest, jqXHR) {
                try {

                    if (jqXHR.readyState == 4) {

                        if (jqXHR.status != 200) {
                            
                            alert('Erro!!!');
                            return;
                        }

                        console.log(data);

                        alert('Entrevista alterada com sucesso!');

                    }

                } catch (e) {

                    ApresentaErro(e, "EditaEntrevista");
                }

            },
            error: function (data, XMLHttpRequest, jqXHR) {

                var msg_erro = "";
                if (data.responseJSON.InnerException == undefined) {

                    msg_erro = data.responseJSON.ExceptionMessage;
                } else {
                    msg_erro = data.responseJSON.ExceptionMessage + "\n" + data.responseJSON.InnerException.ExceptionMessage;
                }

                console.log(msg_erro);

                alert(msg_erro);
            }
        });
    } catch (e) {

        ApresentaErro(e, "EditaEntrevista");
    }
}
function PreencheFormEntrevista(entrevista) {
    try {

        var status_entrevista = entrevista.id_status;

        PreencheOptionSetStatus(status_entrevista);
        
        PreencheOptionSetChances(entrevista.id_chance);

    } catch (e) {

        ApresentaErro(e, "AlteraSenha");
    }
}
function PreencheOptionSetChances(chance_entrevista) {
    try {
        
        GetChances();

        $("#chances").find('option').remove().end();

        for (var pos in array_chances) {

            if (array_chances[pos].id_chance == chance_entrevista) {

                $("#chances").append("<option selected='selected' id= '" + array_chances[pos].id_chance + "' value ='" + array_chances[pos].id_chance + "'  >" + array_chances[pos].descricao + "</option>");
                continue;
            }

            $("#chances").append('<option value = "' + array_chances[pos].id_chance + '">' + array_chances[pos].descricao + '</option>');
        }

    } catch (e) {

        ApresentaErro(e, "PreencheOptionSetChances");
    }
}
function MontaObjetoEntrevista() {
    try {
        entrevista_atual.uf = $("#uf").val();
        entrevista_atual.municipio = $("#municipio").val();
        entrevista_atual.id_status = $("#status").val();
        entrevista_atual.pessoa_entrevistada = $("#responsavel_informacoes").val();
        entrevista_atual.telefone_entrevistado = $("#tel_responsavel").val();
        entrevista_atual.populacao = $("#populacao").val();
        entrevista_atual.eleitores = $("#eleitores").val();
        entrevista_atual.prefeito = $("#prefeito").val();
        entrevista_atual.partido = $("#partido").val();
        entrevista_atual.coligacao = $("#coligacao").val();
        entrevista_atual.id_entrevista = $("#id_entrevista").val();
        entrevista_atual.televisao = $("#televisao").val();
        entrevista_atual.blog = $("#blog").val();
        entrevista_atual.deputado_federal = $("#deputado_federal").val();
        entrevista_atual.meta_vereadores = $("#meta_vereadores").val();
        entrevista_atual.pre_condidato = $("#pre_condidato").val();
        entrevista_atual.id_chance = $("#chances").val();

        var usuario_atual = JSON.parse($.cookie("usuario_logado"));

        entrevista_atual.id_usuario = usuario_atual.id_usuario;

        entrevista_atual.entrevistador = usuario_atual.nome;


        return entrevista_atual;
    }
    catch (e) {

        ApresentaErro(e, "PreencheOptionSetStatus");
    }
}
function ValidaTelefone(nome_campo) {

    if ($("#" + nome_campo).val() == null || $("#" + nome_campo).val() == "") {
        return;
    }
    
    var telefone_formatado = validaTel($("#" + nome_campo).val());

    $("#" + nome_campo).val(telefone_formatado);
}
function ValidouEntrevista() {
    if ($("#uf").val() == null || $("#uf").val() == "") {

        alert("Preencha o campo UF");
        return false;
    }
    if ($("#municipio").val() == null || $("#municipio").val() == "") {

        alert("Preencha o campo Município");
        return false;
    }

    if ($("#responsavel_informacoes").val() == null || $("#responsavel_informacoes").val() == "") {

        alert("Preencha o campo Responsável pelas informações");
        return false;
    }

    if ($("#populacao").val() == null || $("#populacao").val() == "") {

        alert("Preencha o campo População");
        return false;
    }

    if ($("#eleitores").val() == null || $("#eleitores").val() == "") {

        alert("Preencha o campo Eleitores");
        return false;
    }

    if ($("#prefeito").val() == null || $("#prefeito").val() == "") {

        alert("Preencha o campo Prefeito");
        return false;
    }

    if ($("#partido").val() == null || $("#partido").val() == "") {

        alert("Preencha o campo Partido");
        return false;
    }

    return true;

}
function GetChances() {    
    $.ajax({
        crossDomain: true,
        url: url_base + "/api/Chance",
        data: null,
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        cache: true,
        async: false,
        success: function (data, XMLHttpRequest, jqXHR) {
            try {

                if (jqXHR.readyState == 4) {


                    if (jqXHR.status == 200) {

                        console.log(data);
                        
                        array_chances = data;
                    }
                }

            } catch (e) {

                ApresentaErro(e, "GetChances");
            }

        },
        error: function (data, XMLHttpRequest, jqXHR) {


            console.log(data);
        }
    });
}
