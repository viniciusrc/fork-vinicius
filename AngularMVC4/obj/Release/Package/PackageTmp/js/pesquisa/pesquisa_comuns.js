﻿function GetStatus() {

    $.ajax({
        crossDomain: true,
        url: url_base + "/api/Status",
        data: null,
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        cache: true,
        async: false,
        success: function (data, XMLHttpRequest, jqXHR) {
            try {

                if (jqXHR.readyState == 4) {


                    if (jqXHR.status == 200) {

                        console.log(data);

                        array_status = data;
                    }
                }

            } catch (e) {

                ApresentaErro(e, "GetStatus");
            }

        },
        error: function (data, XMLHttpRequest, jqXHR) {


            console.log(data);
        }
    });
}
function PreencheOptionSetStatus(status_entrevista) {
    try {

        GetStatus();
     
        $("#status").find('option').remove().end();

        for (var pos in array_status) {

            if (array_status[pos].id_status == status_entrevista) {

                $("#status").append("<option selected='selected' id= '" + array_status[pos].id_status + "' value ='" + array_status[pos].id_status + "'  >" + array_status[pos].descricao + "</option>");
                continue;
            }

            $("#status").append('<option value = "' + array_status[pos].id_status + '">' + array_status[pos].descricao + '</option>');
        }


    }
    catch (e) {

        ApresentaErro(e, "PreencheOptionSetStatus");
    }
}
