﻿$(function () {
    $('#btn-salvar-senha-nova').click(function () {

        AlteraSenha();
       
    });


});
function AlteraSenha() {
    try {
       
        var ATENCAO = "<strong>Atenção! </strong>";
        var usuario_logado = JSON.parse($.cookie("usuario_logado"));
        
        if ($("#nova_senha").val() == null || $("#nova_senha").val() == "") {
            
            ExibeDivErroCampo(ATENCAO+"Preencha o campo Nova Senha.");
            return;
        } 

        if ($("#confirmacao_nova_senha").val() == null || $("#confirmacao_nova_senha").val() == "") {

           
            ExibeDivErroCampo(ATENCAO + "Preencha o campo Confirme a senha.");
            return;
        }

        if ($("#nova_senha").val() == usuario_logado.senha || $("#confirmacao_nova_senha").val() == usuario_logado.senha) {


            ExibeDivErroCampo(ATENCAO + "Você não mudou a senha antiga.");
            return;
        }

        if ($("#nova_senha").val() !=  $("#confirmacao_nova_senha").val() ) {


            ExibeDivErroCampo(ATENCAO + "Você digitou errado, a confirmação da senha não confere.");
            return;
        }

        $("#erro-campo").hide();

        usuario_logado.trocou_senha = true;

        usuario_logado.data_ultimo_acesso = new Date();

        usuario_logado.senha = $("#confirmacao_nova_senha").val();

        AlteraUsuarioLogado(usuario_logado);

        
    } catch (e) {

        ApresentaErro(e, "AlteraSenha");
    }
}
function ExibeDivErroCampo(msg) {
    $("#erro-campo").html(msg);
    $("#erro-campo").show();
}
function AlteraUsuarioLogado(usuario_logado) {
    try  {
        
        $.ajax({
            crossDomain: true,
            url: url_base + "/api/Usuario/AlteraSenha",
            data: JSON.stringify( usuario_logado),
            type: 'PUT',
            contentType: "application/json; charset=utf-8",
            //dataType: "json",
            // context: {},
            async: false,
            success: function (data, XMLHttpRequest, jqXHR) {
                try {

                    if (jqXHR.readyState == 4) {
                        

                        if (jqXHR.status != 200) {

                            alert('Não alterou senha!');
                            return;
                        }

                        alert('Alterou senha com sucesso!');

                        window.location.href = url_base + "#/bem_vindo";
                    }

                } catch (e) {
                    console.log("O PROBLMEMA É: " + e.message);
                    alert('Não alterou senha!');
                }

            },
            error: function (data, XMLHttpRequest, jqXHR) {

                alert('Não alterou senha!');
                
                var msg_erro = "";
                if (data.responseJSON.InnerException == undefined) {

                    msg_erro = data.responseJSON.ExceptionMessage;
                } else {
                    msg_erro = data.responseJSON.ExceptionMessage + "\n" + data.responseJSON.InnerException.ExceptionMessage;
                }

                console.log(msg_erro);

                alert(msg_erro);
            }
        });

      
    
    } catch (e) {
        
        ApresentaErro(e, "AlteraUsuarioLogado");
    }
}