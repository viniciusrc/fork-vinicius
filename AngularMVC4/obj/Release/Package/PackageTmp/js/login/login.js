﻿$(function () {
    $('#btn-logar').click(function () {

        Logar();
    });

    $('#senha').keydown(function (event) {

        if (event.keyCode == 13/*Enter*/) {
            Logar();
        }

    });

});
function Logar() {

    $("#btn-logar").addClass('disabled');

    $.ajax({
        crossDomain: true,
        url: url_base + "/api/Usuario/PorCredenciais?login=" + $("#login").val(),
        data: JSON.stringify({ login: $("#login").val(), senha: $("#senha").val() }),
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        //dataType: "json",
        // context: {},
        async: false,
        success: function (data, XMLHttpRequest, jqXHR) {
            try {

                if (jqXHR.readyState == 4) {
                  
                    if (jqXHR.status != 200) {
                        return;
                    }

                    console.log(data);
                    if (data == "" || data == null) {
                        alert('Senha ou Usuário inválido!');
                        return;
                    }
                    $("#btn-logar").prop('disabled', false);

                    var usuario = JSON.parse(data);
                    
                    RedirecionaLogin(usuario);
                   
                }

            } catch (e) {
                console.log("O PROBLMEMA É: " + e.message);
                $("#btn-logar").removeClass('disabled');
            }

        },
        error: function (data, XMLHttpRequest, jqXHR) {
            $("#btn-logar").removeClass('disabled');
            
            var msg_erro = "";
            if (data.responseJSON.InnerException == undefined) {

                msg_erro = data.responseJSON.ExceptionMessage;
            } else {
                msg_erro = data.responseJSON.ExceptionMessage + "\n" + data.responseJSON.InnerException.ExceptionMessage;
            }

            console.log(msg_erro);

            alert(msg_erro);
        }
    });


}

function RedirecionaLogin(usuario) {
    try {
        
        $.cookie("usuario_logado", JSON.stringify(usuario) , {
            expires: 1  //expires in 1 days            
        });

        $.cookie("nome_usuario_logado", usuario.nome, {
            expires: 1  //expires in 1 days            
        });

        $("#nome_usuario_logado").html(usuario.nome);

        SairDoDistema(false);        
        
        if (usuario.trocou_senha == false) {           

            window.location.href = url_base + "#/altera_senha";
            return;
        }

        window.location.href = url_base + "#/bem_vindo";

    } catch (e) {
        
        ApresentaErro(e, "RedirecionaLogin");
    }
}
