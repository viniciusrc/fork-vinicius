﻿USE [PSDB]
GO
/****** Object:  Table [dbo].[Chances]    Script Date: 22/05/2016 20:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Chances](
	[id_chance] [int] NOT NULL,
	[descricao] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_chance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Entrevistas]    Script Date: 22/05/2016 20:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Entrevistas](
	[id_entrevista] [int] IDENTITY(1,1) NOT NULL,
	[id_chance] [int] NULL,
	[id_usuario] [int] NOT NULL,
	[id_status] [int] NULL,
	[entrevistador] [varchar](50) NULL,
	[telefone_entrevistado] [varchar](20) NULL,
	[populacao] [int] NULL,
	[eleitores] [int] NULL,
	[prefeito] [varchar](50) NULL,
	[partido] [varchar](20) NULL,
	[coligacao] [varchar](200) NULL,
	[pre_condidato] [bit] NULL,
	[uf] [char](2) NULL,
	[municipio] [varchar](200) NULL,
	[televisao] [bit] NULL,
	[blog] [bit] NULL,
	[deputado_federal] [varchar](50) NULL,
	[meta_vereadores] [tinyint] NULL,
	[pessoa_entrevistada] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_entrevista] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Permissoes]    Script Date: 22/05/2016 20:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Permissoes](
	[id_permissao] [int] NOT NULL,
	[descricao] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_permissao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Status]    Script Date: 22/05/2016 20:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[id_status] [int] NOT NULL,
	[descricao] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoPermissoes]    Script Date: 22/05/2016 20:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoPermissoes](
	[id_permissao] [int] NOT NULL,
	[id_tipo_usuario] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_permissao] ASC,
	[id_tipo_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TiposUsuario]    Script Date: 22/05/2016 20:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TiposUsuario](
	[id_tipo_usuario] [int] NOT NULL,
	[descricao] [varchar](200) NOT NULL,
 CONSTRAINT [PK_TiposUsuario] PRIMARY KEY CLUSTERED 
(
	[id_tipo_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 22/05/2016 20:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id_usuario] [int] IDENTITY(1,1) NOT NULL,
	[id_tipo_usuario] [int] NOT NULL,
	[nome] [varchar](200) NOT NULL,
	[login] [varchar](200) NOT NULL,
	[senha] [varchar](200) NOT NULL,
	[data_ultimo_acesso] [datetime] NULL,
	[trocou_senha] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Chances] ([id_chance], [descricao]) VALUES (10, N'Não Sei')
INSERT [dbo].[Chances] ([id_chance], [descricao]) VALUES (20, N'Vai Ganhar')
INSERT [dbo].[Chances] ([id_chance], [descricao]) VALUES (30, N'Provavel Empate')
INSERT [dbo].[Chances] ([id_chance], [descricao]) VALUES (40, N'Vai perder')
SET IDENTITY_INSERT [dbo].[Entrevistas] ON 

INSERT [dbo].[Entrevistas] ([id_entrevista], [id_chance], [id_usuario], [id_status], [entrevistador], [telefone_entrevistado], [populacao], [eleitores], [prefeito], [partido], [coligacao], [pre_condidato], [uf], [municipio], [televisao], [blog], [deputado_federal], [meta_vereadores], [pessoa_entrevistada]) VALUES (2, 10, 2, 3, N'Wagner dos Santos', N'33422100', 7000, 4500, N'Donizete', N'PSDB', N'PcdB / Pt', 0, N'MG', N'JOAÍMA', 1, 0, N'Márcio Kangussu', 8, N'Luiza Pereira')
SET IDENTITY_INSERT [dbo].[Entrevistas] OFF
INSERT [dbo].[Permissoes] ([id_permissao], [descricao]) VALUES (1, N'Cadastrar Usuário')
INSERT [dbo].[Permissoes] ([id_permissao], [descricao]) VALUES (2, N'Alterar Usuario')
INSERT [dbo].[Permissoes] ([id_permissao], [descricao]) VALUES (3, N'Excluir Usuario')
INSERT [dbo].[Permissoes] ([id_permissao], [descricao]) VALUES (4, N'Visualizar Usuário')
INSERT [dbo].[Permissoes] ([id_permissao], [descricao]) VALUES (5, N'Cadastrar Entrevista')
INSERT [dbo].[Permissoes] ([id_permissao], [descricao]) VALUES (6, N'Alterar Entrevista')
INSERT [dbo].[Permissoes] ([id_permissao], [descricao]) VALUES (7, N'Excluir Entrevista')
INSERT [dbo].[Permissoes] ([id_permissao], [descricao]) VALUES (8, N'Visualizar Entrevista')
INSERT [dbo].[Status] ([id_status], [descricao]) VALUES (1, N'Entrevista Pendente')
INSERT [dbo].[Status] ([id_status], [descricao]) VALUES (2, N'Entrevista Feita')
INSERT [dbo].[Status] ([id_status], [descricao]) VALUES (3, N'Entrevista Em andamento')
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (1, 10)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (1, 30)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (2, 10)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (2, 30)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (3, 10)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (4, 10)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (4, 20)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (4, 30)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (5, 10)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (5, 30)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (6, 10)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (6, 30)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (7, 10)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (8, 10)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (8, 20)
INSERT [dbo].[TipoPermissoes] ([id_permissao], [id_tipo_usuario]) VALUES (8, 30)
INSERT [dbo].[TiposUsuario] ([id_tipo_usuario], [descricao]) VALUES (10, N'Administrador Sistema')
INSERT [dbo].[TiposUsuario] ([id_tipo_usuario], [descricao]) VALUES (20, N'Pesquisador')
INSERT [dbo].[TiposUsuario] ([id_tipo_usuario], [descricao]) VALUES (30, N'Cadastrador')
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([id_usuario], [id_tipo_usuario], [nome], [login], [senha], [data_ultimo_acesso], [trocou_senha]) VALUES (1, 10, N'Administrador do Sistema', N'admin', N'789', CAST(N'2016-05-22 19:45:59.563' AS DateTime), 1)
INSERT [dbo].[Usuarios] ([id_usuario], [id_tipo_usuario], [nome], [login], [senha], [data_ultimo_acesso], [trocou_senha]) VALUES (2, 30, N'Wagner dos Santos', N'wag', N'123', CAST(N'2016-05-22 19:43:58.743' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
ALTER TABLE [dbo].[Entrevistas]  WITH CHECK ADD FOREIGN KEY([id_chance])
REFERENCES [dbo].[Chances] ([id_chance])
GO
ALTER TABLE [dbo].[Entrevistas]  WITH CHECK ADD FOREIGN KEY([id_status])
REFERENCES [dbo].[Status] ([id_status])
GO
ALTER TABLE [dbo].[Entrevistas]  WITH CHECK ADD FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id_usuario])
GO
ALTER TABLE [dbo].[TipoPermissoes]  WITH CHECK ADD FOREIGN KEY([id_permissao])
REFERENCES [dbo].[Permissoes] ([id_permissao])
GO
ALTER TABLE [dbo].[TipoPermissoes]  WITH CHECK ADD FOREIGN KEY([id_tipo_usuario])
REFERENCES [dbo].[TiposUsuario] ([id_tipo_usuario])
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD FOREIGN KEY([id_tipo_usuario])
REFERENCES [dbo].[TiposUsuario] ([id_tipo_usuario])
GO
