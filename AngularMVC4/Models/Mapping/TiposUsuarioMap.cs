using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AngularMVC4.Models.Mapping
{
    public class TiposUsuarioMap : EntityTypeConfiguration<TiposUsuario>
    {
        public TiposUsuarioMap()
        {
            // Primary Key
            this.HasKey(t => t.id_tipo_usuario);

            // Properties
            this.Property(t => t.id_tipo_usuario)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.descricao)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("TiposUsuario");
            this.Property(t => t.id_tipo_usuario).HasColumnName("id_tipo_usuario");
            this.Property(t => t.descricao).HasColumnName("descricao");
        }
    }
}
