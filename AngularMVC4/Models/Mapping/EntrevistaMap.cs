using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AngularMVC4.Models.Mapping
{
    public class EntrevistaMap : EntityTypeConfiguration<Entrevista>
    {
        public EntrevistaMap()
        {
            // Primary Key
            this.HasKey(t => t.id_entrevista);

            // Properties
            this.Property(t => t.entrevistador)
                .HasMaxLength(50);

            this.Property(t => t.telefone_entrevistado)
                .HasMaxLength(20);

            this.Property(t => t.prefeito)
                .HasMaxLength(50);

            this.Property(t => t.partido)
                .HasMaxLength(20);

            this.Property(t => t.coligacao)
                .HasMaxLength(200);

            this.Property(t => t.uf)
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.municipio)
                .HasMaxLength(200);

            this.Property(t => t.deputado_federal)
                .HasMaxLength(50);

            this.Property(t => t.pessoa_entrevistada)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Entrevistas");
            this.Property(t => t.id_entrevista).HasColumnName("id_entrevista");
            this.Property(t => t.id_chance).HasColumnName("id_chance");
            this.Property(t => t.id_usuario).HasColumnName("id_usuario");
            this.Property(t => t.id_status).HasColumnName("id_status");
            this.Property(t => t.entrevistador).HasColumnName("entrevistador");
            this.Property(t => t.telefone_entrevistado).HasColumnName("telefone_entrevistado");
            this.Property(t => t.populacao).HasColumnName("populacao");
            this.Property(t => t.eleitores).HasColumnName("eleitores");
            this.Property(t => t.prefeito).HasColumnName("prefeito");
            this.Property(t => t.partido).HasColumnName("partido");
            this.Property(t => t.coligacao).HasColumnName("coligacao");
            this.Property(t => t.pre_condidato).HasColumnName("pre_condidato");
            this.Property(t => t.uf).HasColumnName("uf");
            this.Property(t => t.municipio).HasColumnName("municipio");
            this.Property(t => t.televisao).HasColumnName("televisao");
            this.Property(t => t.blog).HasColumnName("blog");
            this.Property(t => t.deputado_federal).HasColumnName("deputado_federal");
            this.Property(t => t.meta_vereadores).HasColumnName("meta_vereadores");
            this.Property(t => t.pessoa_entrevistada).HasColumnName("pessoa_entrevistada");

            // Relationships
           

        }
    }
}
