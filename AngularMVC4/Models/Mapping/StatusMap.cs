using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AngularMVC4.Models.Mapping
{
    public class StatusMap : EntityTypeConfiguration<Status>
    {
        public StatusMap()
        {
            // Primary Key
            this.HasKey(t => t.id_status);

            // Properties
            this.Property(t => t.id_status)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.descricao)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Status");
            this.Property(t => t.id_status).HasColumnName("id_status");
            this.Property(t => t.descricao).HasColumnName("descricao");
        }
    }
}
