using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AngularMVC4.Models.Mapping
{
    public class PermissoMap : EntityTypeConfiguration<Permisso>
    {
        public PermissoMap()
        {
            // Primary Key
            this.HasKey(t => t.id_permissao);

            // Properties
            this.Property(t => t.id_permissao)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.descricao)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Permissoes");
            this.Property(t => t.id_permissao).HasColumnName("id_permissao");
            this.Property(t => t.descricao).HasColumnName("descricao");

            // Relationships
            this.HasMany(t => t.TiposUsuarios)
                .WithMany(t => t.Permissoes)
                .Map(m =>
                    {
                        m.ToTable("TipoPermissoes");
                        m.MapLeftKey("id_permissao");
                        m.MapRightKey("id_tipo_usuario");
                    });


        }
    }
}
