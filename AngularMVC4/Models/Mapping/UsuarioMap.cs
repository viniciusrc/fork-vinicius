using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AngularMVC4.Models.Mapping
{
    public class UsuarioMap : EntityTypeConfiguration<Usuario>
    {
        public UsuarioMap()
        {
            // Primary Key
            this.HasKey(t => t.id_usuario);

            // Properties
            this.Property(t => t.nome)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.login)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.senha)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Usuarios");
            this.Property(t => t.id_usuario).HasColumnName("id_usuario");
            this.Property(t => t.id_tipo_usuario).HasColumnName("id_tipo_usuario");
            this.Property(t => t.nome).HasColumnName("nome");
            this.Property(t => t.login).HasColumnName("login");
            this.Property(t => t.senha).HasColumnName("senha");
            this.Property(t => t.data_ultimo_acesso).HasColumnName("data_ultimo_acesso");
            this.Property(t => t.trocou_senha).HasColumnName("trocou_senha");

            // Relationships
            //this.HasRequired(t => t.TiposUsuario)
            //    .WithMany(t => t.Usuarios)
            //    .HasForeignKey(d => d.id_tipo_usuario);

        }
    }
}
