using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AngularMVC4.Models.Mapping
{
    public class ChanceMap : EntityTypeConfiguration<Chance>
    {
        public ChanceMap()
        {
            // Primary Key
            this.HasKey(t => t.id_chance);

            // Properties
            this.Property(t => t.id_chance)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.descricao)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Chances");
            this.Property(t => t.id_chance).HasColumnName("id_chance");
            this.Property(t => t.descricao).HasColumnName("descricao");
        }
    }
}
