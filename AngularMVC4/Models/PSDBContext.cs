using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using AngularMVC4.Models.Mapping;

namespace AngularMVC4.Models
{
    public partial class PSDBContext : DbContext
    {
        static PSDBContext()
        {
            Database.SetInitializer<PSDBContext>(null);
        }

        public PSDBContext()
            : base("Name=PSDBContext")
        {
        }

        public DbSet<Chance> Chances { get; set; }
        public DbSet<Entrevista> Entrevistas { get; set; }
        public DbSet<Permisso> Permissoes { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<TiposUsuario> TiposUsuarios { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ChanceMap());
            modelBuilder.Configurations.Add(new EntrevistaMap());
            modelBuilder.Configurations.Add(new PermissoMap());
            modelBuilder.Configurations.Add(new StatusMap());
            modelBuilder.Configurations.Add(new TiposUsuarioMap());
            modelBuilder.Configurations.Add(new UsuarioMap());
        }
    }
}
