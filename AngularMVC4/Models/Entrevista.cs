using System;
using System.Collections.Generic;

namespace AngularMVC4.Models
{
    public partial class Entrevista
    {
        public int id_entrevista { get; set; }
        public Nullable<int> id_chance { get; set; }
        public int id_usuario { get; set; }
        public Nullable<int> id_status { get; set; }
        public string entrevistador { get; set; }
        public string telefone_entrevistado { get; set; }
        public Nullable<int> populacao { get; set; }
        public Nullable<int> eleitores { get; set; }
        public string prefeito { get; set; }
        public string partido { get; set; }
        public string coligacao { get; set; }
        public Nullable<bool> pre_condidato { get; set; }
        public string uf { get; set; }
        public string municipio { get; set; }
        public Nullable<bool> televisao { get; set; }
        public Nullable<bool> blog { get; set; }
        public string deputado_federal { get; set; }
        public Nullable<byte> meta_vereadores { get; set; }
        public string pessoa_entrevistada { get; set; }
       
    }
}
