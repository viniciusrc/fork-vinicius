using System;
using System.Collections.Generic;

namespace AngularMVC4.Models
{
    public partial class Permisso
    {
        public Permisso()
        {
            this.TiposUsuarios = new List<TiposUsuario>();
        }

        public int id_permissao { get; set; }
        public string descricao { get; set; }
        public virtual ICollection<TiposUsuario> TiposUsuarios { get; set; }
    }
}
