using System;
using System.Collections.Generic;

namespace AngularMVC4.Models
{
    public partial class TiposUsuario
    {
        public TiposUsuario()
        {
            this.Usuarios = new List<Usuario>();
            this.Permissoes = new List<Permisso>();
        }

        public int id_tipo_usuario { get; set; }
        public string descricao { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
        public virtual ICollection<Permisso> Permissoes { get; set; }
    }
}
