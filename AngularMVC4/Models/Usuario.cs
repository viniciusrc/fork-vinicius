using System;
using System.Collections.Generic;

namespace AngularMVC4.Models
{
    public partial class Usuario
    {
        public Usuario()
        {
           // this.Entrevistas = new List<Entrevista>();
        }

        public int id_usuario { get; set; }
        public int id_tipo_usuario { get; set; }
        public string nome { get; set; }
        public string login { get; set; }
        public string senha { get; set; }
        public Nullable<System.DateTime> data_ultimo_acesso { get; set; }
        public Nullable<bool> trocou_senha { get; set; }
        //public virtual ICollection<Entrevista> Entrevistas { get; set; }
        //public virtual TiposUsuario TiposUsuario { get; set; }
    }
}
