﻿
ws_app = angular.module('ws_app', ['ngRoute']);

ws_app
        .config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
            $routeProvider
                    .when('/', {
                        templateUrl: 'Pags/login/login.html'
                    })
                    .when('/login', {
                        templateUrl: 'Pags/login/login.html'
                    })
                    .when('/altera_senha', {
                        templateUrl: 'Pags/login/altera_senha.html'
                    })
                    .when('/bem_vindo', {
                        templateUrl: 'Pags/login/bem_vindo.html'
                    })
                    .when('/pesquisas', {
                        templateUrl: 'Pags/pesquisa/pesquisa.html',
                        controller: 'ctrl_pesquisa'
                    }).when('/pesquisa/:index', {
                        templateUrl: 'Pags/pesquisa/pesquisa_visualizar.html',
                        controller: 'ctrl_entrevista_update'
                    })
                    .when('/nova_entrevista', {
                        templateUrl: 'Pags/pesquisa/pesquisa_visualizar.html',
                        controller: 'ctrl_entrevista_nova'
                        })
                    .when('/usuarios', {
                        templateUrl: 'Pags/usuario/listagem_usuario.html',
                        controller: 'ctrl_usuario'
                    }).when('/usuario/:index', {
                        templateUrl: 'Pags/usuario/usuario_visualizar.html',
                        controller: 'ctrl_usuario_update'
                    })
                    .when('/novo_usuario', {
                        templateUrl: 'Pags/usuario/usuario_visualizar.html',
                        controller: 'ctrl_usuario_novo'
                    })
                    .when('/usuario_excluir/:index', {
                        templateUrl: 'Pags/usuario/listagem_usuario.html',
                        controller: 'ctrl_usuario_deletar'
                    }).when('/tipo_usuarios', {
                        templateUrl: 'Pags/tipo_usuario/listagem_tipo_usuario.html',
                        controller: 'ctrl_tipo_usuario'
                    }).when('/tipo_usuarios/:index', {
                        templateUrl: 'Pags/tipo_usuario/tipo_usuario_visualizar.html',
                        controller: 'ctrl_tipo_usuario'
                    }).when('/tipo_usuarios/novo', {
                        templateUrl: 'Pags/tipo_usuario/tipo_usuario_visualizar.html',
                        controller: 'ctrl_tipo_usuario'
                    })
                   .otherwise({ redirectTo: '/' }); 

//configura o RESPONSE interceptor, usado para exibir o ícone de acesso ao servidor
// e a exibir uma mensagem de erro caso o servidor retorne algum erro
$httpProvider.interceptors.push(function($q,$rootScope) {
    return function(promise) {
        //Always disable loader
        $rootScope.hideLoader();
        return promise.then(function(response) {
            // do something on success
            return(response);
        }, function (response) {
            
            // do something on error
            $data = response.data;
            $error = $data.error;
            console.error($data);
            if ($error && $error.text)
                alert("ERROR: " + $error.text);
            else{
                if (response.status=404)
                    alert("Erro ao acessar servidor. Página não encontrada. Veja o log de erros para maiores detalhes");
                else
                    alert("ERROR! See log console");
            }
            return $q.reject(response);
        });
    }
});      

}]);	


ws_app.run(['$rootScope', function ($rootScope) {

    //Uma flag que define se o ícone de acesso ao servidor deve estar ativado
    $rootScope.showLoaderFlag = false;

    //Força que o ícone de acesso ao servidor seja ativado
    $rootScope.showLoader = function () {
        $rootScope.showLoaderFlag = true;
    }
    //Força que o ícone de acesso ao servidor seja desativado
    $rootScope.hideLoader = function () {
        $rootScope.showLoaderFlag = false;
    }

    //Método que retorna a URL completa de acesso ao servidor. 
    // Evita usar concatenação
    $rootScope.server = function (url) {
        return SERVER_URL + url;
    }

}]);

//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
ws_app.filter('startFrom', function () {
    return function (input, start) {
        if (input == null)
            return null;
        start = +start; //parse to int
        return input.slice(start);
    }
});
