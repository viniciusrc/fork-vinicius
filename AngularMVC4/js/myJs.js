﻿
url_base = "http://localhost:8838";
//url_base = "http://192.168.0.103";


$(function () {

    $('#div-botao-sair').click(function () {

        SairDoDistema(true);
    });


});

function ApresentaErro(execao, nome_funcao) {

    var msg_erro = 'Erro na função: ' + nome_funcao + "\n" + execao.message
    console.log(msg_erro);
    console.log(execao.name);
    alert(msg_erro);
}
function SairDoDistema(sair) {
    
    if (sair == true || $.cookie("nome_usuario_logado") == undefined) {

        $("#logo_principal").hide();

        $("#div-botao-sair").hide();

        $("#nome_usuario_logado").html("");

        $.removeCookie('usuario_logado');

        $.removeCookie('nome_usuario_logado');

    } else {

        $("#logo_principal").show();

        $("#div-botao-sair").show();

        $("#nome_usuario_logado").html($.cookie("nome_usuario_logado"));

    }

}
function validaTel(valor) {

    var valida1 = /^([\d]{4})([\d]{4})$/;
    
    if (valida1.test(valor)) {

        valor = valor.replace(valida1, "+55 (31)$1-$2");
        return valor;
    }

    var valida2 = /^9([\d]{4})([\d]{4})$/;

    if (valida2.test(valor)) {

        valor = valor.replace(valida2, "+55 (31)9$1-$2");
        return valor;
    }

    var valida3 = /^([\d]{2})([\d]{4})([\d]{4})$/;

    if (valida3.test(valor)) {

        valor = valor.replace(valida3, "+55 ($1)$2-$3");
        return valor;
    }

    var valida4 = /^([\d]{2})9([\d]{4})([\d]{4})$/;

    if (valida4.test(valor)) {

        valor = valor.replace(valida4, "+55 ($1)9$2-$3");
        return valor;
    }

    var valida5 = /^([\d]{2})([\d]{2})([\d]{4})([\d]{4})$/;

    if (valida5.test(valor)) {

        valor = valor.replace(valida5, "+$1 ($2)$3-$4");
        return valor;
    }

    var valida6 = /^([\d]{2})([\d]{2})9([\d]{4})([\d]{4})$/;

    if (valida6.test(valor)) {

        valor = valor.replace(valida6, "+$1 ($2)9$3-$4");
        return valor;
    }

    var valida7 = /^\+([\d]{2}) (\([\d]{2}\))([\d]{4})-([\d]{4})$/;

    if (valida7.test(valor)) {

        return valor;
    }

    var valida8 = /^\+([\d]{2}) (\([\d]{2}\))9([\d]{4})-([\d]{4})$/;

    if (valida8.test(valor)) {

        return valor;
    }

    var valida9 = /^\+55 (31)([\d]{4})-([\d]{4})$/;

    if (valida9.test(valor)) {

        return valor;
    }


    if (valor == "" || null) {
        return null;
    }

    alert('Numero de Telefone Invlálido');

    return null;

}
function ResetaFormulario(id_formulario) {

    $('#' + id_formulario).each(function () {
        this.reset();
    });
}
function GetDummy() {

    var dummy = "&dummy=" + new Date().getTime();

    return dummy;
}