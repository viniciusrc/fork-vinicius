﻿
ws_app.controller('ctrl_usuario', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {

    $scope.paginaAtual = 0;
    $scope.tamanhoPagina = 4;

    $scope.numberOfPages = function () {
        return Math.ceil($scope.usuarios.length / $scope.tamanhoPagina);
    }

    $scope.loadAll = function () {

        $http.get(url_base + "/api/Usuario").success(function (data, status, fun, header) {

            $scope.usuarios = data;
        }).error(function (data, status, fun, header) {

            console.log("PROBLEMA ==>: " + data);
        });
    }

    $scope.listarEspecificos = function () {

        $scope.usuarios = [];

        var tipo = $("#tipo").val();
        var login = ($scope.login != undefined) ? $scope.login : "";
        var nome = ($scope.nome != undefined) ? $scope.nome : "";
        
        $http.get(url_base + "/api/Usuario/Especifico?login=" + login + "&nome=" + nome + "&tipo=" + tipo + GetDummy()).success(function (data, status, fun, header) {
                        
            $scope.usuarios = data;
            debugger;
            setTimeout(function () {
                GetTipoUsuarios();                
            }, 1300);

        }).error(function (data, status, fun, header) {
            
            console.log("PROBLEMA ==>: " + data.MessageDetail);
            alert("PROBLEMA ==>: " + data.MessageDetail);
        });
    }

    $scope.limparCampos = function () {
        $scope.login = null;
        $scope.nome = null;
    }

}]).controller('ctrl_usuario_update', function ($scope, $http, $routeParams) {


    $http.get(url_base + "/api/Usuario/UsuarioPorId?id_usuario=" + $routeParams.index).success(function (data, status, fun, header) {

        $scope.usuario = data;

        PreencheOtpionSetsUsuario($scope.usuario);//From usuario.js

        ExibePermissoesTexto();//From usuario.js

    }).error(function (data, status, fun, header) {

        console.log("PROBLEMA ==>: " + data);
    });

}).controller('ctrl_usuario_novo', function ($scope, $http, $routeParams) {

    $scope.entrevista = null;
    debugger;
    var novo_usuario = new Object();

    novo_usuario.id_tipo_usuario = 20;//persquisador

    PreencheOtpionSetsUsuario(novo_usuario);//From usuario.js

    ExibePermissoesTexto();//From usuario.js
}).controller('ctrl_usuario_deletar', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {

    if (!confirm("Deseja Deletar este registro?")) {
        window.location.href = url_base + "#/usuarios";
        return;
    }

    $http.delete(url_base + "/api/Usuario/Deletar?id_usuario=" + $routeParams.index).success(function (data, status, fun, header) {

        window.location.href = url_base + "#/usuarios";

    }).error(function (data, status, fun, header) {

        console.log("PROBLEMA ==>: " + data);
    });

}]);
