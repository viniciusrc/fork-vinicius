﻿
angular.module('ws_app').factory('tipo_usuario_service', function ($http) {



    return {
        listar: function () {

            return $http.get(url_base + "/api/Tipo");
        },

        buscarPorId: function (id) {

            if (id == undefined) {
                return;
            }

            id = parseInt(id);
            return $http.get(url_base + "/api/Tipo/" + id);
        },

        alterar: function (tipo_usuario) {

            return $http.put(url_base + "/api/Tipo", tipo_usuario);
        },

        criar: function (tipo_usuario) {

            return $http.post(url_base + "/api/Tipo", tipo_usuario);
        },
        deletar: function (id) {

            return $http.delete(url_base + "/api/Tipo/" + id);
        }
    }

});
