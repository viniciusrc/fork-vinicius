﻿
ws_app.controller("ctrl_tipo_usuario", function ($scope, $route, $http, $routeParams, $location, tipo_usuario_service) {


    $scope.loadAll = function () {

        var promise = tipo_usuario_service.listar();

        promise.then(function (response) {

            console.log(response.data);
            $scope.tipos_usuarios = response.data;

        }).catch(function (response) {
            console.log(response);

            alert(response.data.Message);
        })

    };

    $scope.loadOne = function () {

        if ($routeParams.index == "novo") {
            return;
        }

        var promise = tipo_usuario_service.buscarPorId($routeParams.index);

        promise.then(function (response) {

            console.log(response.data);

            $scope.tipo_usuario = response.data;
        }).catch(function (response) {
            console.log(response);

            alert(response.data.Message);
        })
    },

    $scope.saveRegister = function (tipo_usuario) {

         if (tipo_usuario.id_tipo_usuario != undefined) {

             editaTipo(tipo_usuario_service, tipo_usuario, $location);
         } else {
             criaNovoTipo(tipo_usuario_service, tipo_usuario, $location);
         }
     },

    $scope.deleteOne = function (id) {

            if (id == undefined) {
                return;
            }
           
            var promise = tipo_usuario_service.deletar(id);

            $route.reload();
        }

});

function criaNovoTipo(tipo_usuario_service, tipo_usuario, $location) {

    var promise = tipo_usuario_service.criar(tipo_usuario);

    ProcessaResultadoRequest(promise, $location);
}
function editaTipo(tipo_usuario_service, tipo_usuario, $location) {

    var promise = tipo_usuario_service.alterar(tipo_usuario);

    ProcessaResultadoRequest(promise, $location);
}

function ProcessaResultadoRequest(promise, $location) {

    promise.then(function (response) {

        console.log(response.data);

        $location.path('/tipo_usuarios');

    }).catch(function (response) {
        console.log(response);

        alert(response.data.Message);
    })
}
